package com.company.Consumer;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private LinkedBlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    public boolean closed=false;
    public Server(int capacity){
        tasks=new LinkedBlockingQueue<Task>(capacity);
        waitingPeriod=new AtomicInteger();
    }
    public void addTask(Task newTask){
        tasks.add(newTask);
        waitingPeriod.incrementAndGet();
    }
    public void run(){
        int ct=0,ok=1;
        while(true){
            try {
                if(this.closed)
                    return;
                if(tasks.peek()!=null) {
                    Task task = tasks.peek();
                    task.setProcessingTime(task.getProccessingTime() - 1);
                    if(task.getProccessingTime()==0) {
                        tasks.remove(task);
                        waitingPeriod.decrementAndGet();
                    }
                    Thread.sleep(1000);
                    }
                else
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public LinkedBlockingQueue<Task> getTasks(){
        return tasks;
    }
}
