package com.company.Strategy;

import com.company.Consumer.Server;
import com.company.Consumer.Task;

import java.util.List;

public interface Strategy {
    public void addTask(List<Server> servers, Task t);
}
