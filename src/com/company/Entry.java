package com.company;

import com.company.Producer.SimulationManager;
import com.company.utils.MyFile;

import java.io.IOException;

public class Entry {

    public static void main(String[] args) throws IOException {
		String input=args[0];
		String output=args[1];
		System.out.println(input);
		MyFile myFile=new MyFile(input,output);
		SimulationManager gen=new SimulationManager(myFile);
		Thread t=new Thread(gen);
		t.start();
    }
}
