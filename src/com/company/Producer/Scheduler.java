package com.company.Producer;

import com.company.Strategy.SelectionPolicy;
import com.company.Consumer.Server;
import com.company.Strategy.ConcreteStrategyQueue;
import com.company.Strategy.ConcreteStrategyTime;
import com.company.Strategy.Strategy;
import com.company.Consumer.Task;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers   ,int maxTasksPerServer){
        this.maxNoServers=maxNoServers;
        this.maxTasksPerServer=maxTasksPerServer;
        servers=new ArrayList<>();
        for(int i=0;i<maxNoServers;i++){
            Server server=new Server(maxTasksPerServer);
            this.servers.add(server);
            new Thread(server).start();


        }
    }
    public void createStrategy(SelectionPolicy policy){
        if(policy==SelectionPolicy.SHORTEST_QUEUE)
            strategy=new ConcreteStrategyQueue();
        if(policy==SelectionPolicy.SHORTEST_TIME)
            strategy=new ConcreteStrategyTime();
    }
    public void dispatchTask(Task t){
        strategy.addTask(servers,t);
    }
    public List<Server> getServers() {
        return servers;
    }
}
