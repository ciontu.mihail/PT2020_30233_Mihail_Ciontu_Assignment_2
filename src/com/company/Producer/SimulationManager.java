package com.company.Producer;

import com.company.Consumer.Server;
import com.company.Consumer.Task;
import com.company.Strategy.SelectionPolicy;
import com.company.utils.MyFile;

import java.io.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class SimulationManager implements Runnable{
    FileWriter fw;
    public int timeLimit;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int maxServiceTime;
    public int minServiceTime;
    public int numberOfServers;
    public int numberOfClients;

    public MyFile myFile;
    public SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_TIME;

    private Scheduler scheduler;

    private List<Task> generatedTasks=new ArrayList<>();


    public SimulationManager(MyFile myFile) throws IOException {
        this.myFile=myFile;
        fw=myFile.getWriter();
        initializeInfo();
        scheduler=new Scheduler(numberOfServers,10);
        scheduler.createStrategy(selectionPolicy);
        generateRandomTasks();
        Collections.sort(generatedTasks,new SortByArrival());

    }

    public void run() {
        int currentTime=0;
        double serviceTimeAverage=0;
        while(currentTime<timeLimit){
                if(generatedTasks.size()!=0) {
                    if (generatedTasks.get(0).getArrivalTime() <= currentTime) {
                        scheduler.dispatchTask(generatedTasks.get(0));
                        serviceTimeAverage+=generatedTasks.get(0).getProccessingTime();
                        generatedTasks.remove(0);

                    }
                }
                else
                {
                    int ct=0;
                    for(int i=0;i<numberOfServers;i++){
                        if(scheduler.getServers().get(i).getTasks().size()==0 ) {
                            ct++;
                        }
                    }
                    if(ct==numberOfServers) {
                        for(int i=0;i<numberOfServers;i++) {
                            scheduler.getServers().get(i).closed = true;
                        }

                        serviceTimeAverage=serviceTimeAverage/(double)numberOfClients;
                        try {
                            fw.write("Service Time Average="+serviceTimeAverage);
                            System.out.println("Service Time Average="+serviceTimeAverage);
                            fw.flush();
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                }
            try {
                fw.write("Time=");
                fw.write(currentTime+"\r");
                System.out.println("Time="+currentTime);
                List<Server> servers=scheduler.getServers();
                System.out.println("Waiting Clients:");
                fw.write("Waiting Clients:"+"\r");
                for(Task task:generatedTasks){
                    String taskInfo=task.getID()+","+task.getProccessingTime()+","+task.getArrivalTime();
                    System.out.println(taskInfo);
                    fw.write("("+taskInfo+")");
                }
                fw.write("\r");
                for(int i=0;i<numberOfServers;i++){
                    fw.write("Queue"+i+":");
                    System.out.println("Queue"+i+":");

                    LinkedBlockingQueue<Task> tasks=servers.get(i).getTasks();
                    Iterator<Task> ts=tasks.iterator();
                    if(!ts.hasNext()){
                        fw.write("closed"+"\r");
                        continue;
                    }
                    while(ts.hasNext()){
                        Task task=ts.next();
                        String taskInfo=task.getID()+","+task.getProccessingTime()+","+task.getArrivalTime();
                        System.out.println(taskInfo);
                        fw.write("("+taskInfo+")");
                    }
                    fw.write("\n");
                }
                currentTime++;
                Thread.sleep(1000);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }

    }
    private void generateRandomTasks(){
        for(int i=1;i<=numberOfClients;i++) {
            generatedTasks.add(new Task(i,
                    getRandomNumberInRange(minProcessingTime,maxProcessingTime),
                    getRandomNumberInRange(minServiceTime,maxServiceTime)));
        }
    }
    public void initializeInfo(){
        List<Integer> myInfo=myFile.getFileInformation();
        System.out.println(myInfo);
        numberOfClients=myInfo.get(0);
        numberOfServers=myInfo.get(1);
        timeLimit=myInfo.get(2);
        minProcessingTime=myInfo.get(3);
        maxProcessingTime=myInfo.get(4);
        minServiceTime=myInfo.get(5);
        maxServiceTime=myInfo.get(6);
    }
    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}

class SortByArrival implements Comparator<Task>
{
    public int compare(Task a, Task b)
    {
        return a.getArrivalTime() - b.getArrivalTime();
    }
}
