package com.company.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyFile {
    Scanner fr;
    FileWriter fw;
    File file;
    List<Integer> fileInfo;
    public MyFile(String inputName,String outputName) throws IOException {
        fileInfo=new ArrayList<>();
        file=new File(outputName);
        try {
            fr = new Scanner(new File("../../../"+inputName));
            while(fr.hasNextInt()){
                fileInfo.add(fr.nextInt());
            }

        }catch (FileNotFoundException fe){
            System.out.println("File not found");
        }
        file.delete();

        fw=new FileWriter(outputName,true);
    }
    public FileWriter getWriter(){
        return fw;
    }
    public List<Integer> getFileInformation(){
        return fileInfo;
    }
}
